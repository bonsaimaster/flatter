import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class Email:
    def __init__(self):
        self._get_account_info()

    def _get_account_info(self):
        """ Requires file 'account_info' to be present with username/password."""
        with open("account_info", "r") as file:
            lines=file.readlines()
            self.gmail_address=lines[0]
            self.gmail_password=lines[1]
            # TODO: Verify if file exists

    def _prepare_message(self, send_to, subject, body):
        msg = MIMEMultipart()
        msg["To"] = send_to
        msg["From"] = self.gmail_address
        msg["Subject"] = subject
        msg.attach(MIMEText(body, 'plain'))
        return msg.as_string()

    def send(self, send_to, subject, body):
        message = self._prepare_message(send_to, subject, body)

        try:
            server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
            server.ehlo()
            server.login(self.gmail_address, self.gmail_password)
            server.sendmail(self.gmail_address, send_to, message)
            server.close()

            print('Email sent!')
        except Exception as e:
            print('Something went wrong...')
            raise e
