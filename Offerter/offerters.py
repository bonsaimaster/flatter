from .offerter import Offerter

class Gratka(Offerter):
    def scrap_website(self):
        articles = self.parsed_html.find_all('article', 'teaserUnified')
        if articles:
            for article in articles:
                id = article.get("id").split("-")[1]
                href = article.get("data-href")
                self.scrapped_articles[id] = {"link":href}


class Gumtree(Offerter):
    def scrap_website(self):
        view = self.parsed_html.find(class_="view")
        if view:
            tiles = view.find_all(class_="tileV1")
            for tile in tiles:
                id = tile.find(class_="addAdTofav").get("data-adid")
                href = tile.find(class_="href-link").get("href")
                link = "https://www.gumtree.pl" + href
                self.scrapped_articles[id] = {"link":link}


class Otodom(Offerter):
    def scrap_website(self):
        articles = self.parsed_html.find_all('article')
        if articles:
            for article in articles:
                id = article.get("data-tracking-id")
                href = article.get("data-url")
                self.scrapped_articles[id] = {"link":href}


class Morizon(Offerter):
    def scrap_website(self):
        list = self.parsed_html.find(class_="mainBox").section
        if list:
            articles = list.find_all("div", recursive=False)
            for article in articles:
                if article.has_attr("data-id"):
                    id = article.get("data-id")
                    href = article.find(class_="property_link").get("href")
                    self.scrapped_articles[id] = {"link":href}


class Oferty(Offerter):
    def scrap_website(self):
        articles = self.parsed_html.find_all(class_='property')
        if articles:
            for article in articles:
                id = article.get("id").lstrip("id_")
                href = article.find("a").get("href")
                self.scrapped_articles[id] = {"link":href}

class Nieruchomosci(Offerter):
    def scrap_website(self):
        # Promoted articles case
        promoted = self.parsed_html.find(id="pie_prime")
        if promoted:
            # Promoted articles
            promoted = promoted.next_sibling.find_all(attrs={"data-id": True})
            for article in promoted:
                id = article.get("data-id").lstrip("a")
                href = article.find("a").get("href")
                self.scrapped_articles[id] = {"link":href}
            # Regular articles
            normal = self.parsed_html.find(id="pie_normal").next_sibling.find_all(attrs={"data-id": True})
            for article in normal:
                id = article.get("data-id").lstrip("a")
                href = article.find("a").get("href")
                self.scrapped_articles[id] = {"link":href}
        else:        
            # No promoted articles case
            articles = self.parsed_html.find(id="tilesWrapper").find("div", "column-container").find_all(attrs={"data-id": True})
            for article in articles:
                id = article.get("data-id").lstrip("a")
                href = article.find("a").get("href")
                self.scrapped_articles[id] = {"link":href}
        

class Olx(Offerter):
    def scrap_website(self):
        articles = self.parsed_html.find_all(summary="Ogłoszenie")
        for article in articles:
            id = article.get("data-id")
            href = article.find("a").get("href")
            self.scrapped_articles[id] = {"link":href}