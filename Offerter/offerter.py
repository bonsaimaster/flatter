from abc import ABC, abstractmethod
from bs4 import BeautifulSoup
import json
import logging
import os
import re
import requests
from requests.exceptions import RequestException
from fake_useragent import UserAgent

class Offerter(ABC):
    def __init__(self, link, name=None):
        self.link = link
        self.parsed_html = None
        self.scrapped_articles = {}
        self.new_articles = {}
        
        if name:
            self.name = name
        else:
            self.name = self.__class__.__name__
        self.json_filename = "{}.json".format(self.name)
        self.json_path = "json/{}".format(self.json_filename)

    @abstractmethod
    def scrap_website(self):
        """ Make sure to add scrapped articles in following format:
            {id:{"link":href<str>}
            e.g self.scrapped_articles[id] = {"link":href}"""
        pass

    def __str__(self):
        s = "\n{}({})\n".format(self.name, len(self.new_articles))
        if self.new_articles:
            for id in self.new_articles:
                s = s + "{}\n".format(self.new_articles[id]["link"])
        return s

    def parse_html(self):
        ua = UserAgent()
        try:
            response = requests.get(url=self.link, headers={'User-Agent':str(ua.firefox)})
            if response:
                self.parsed_html = BeautifulSoup(response.text, 'html.parser')
                logging.debug("Soup created successfully.")
        except RequestException:
            logging.warning("A HTTP error has occured during making a soup.")

    def json_exists(self):
        if os.path.exists(self.json_path):
            logging.debug("JSON file {} exists.".format(self.json_path))
            return True
        else:
            logging.debug("JSON file {} does not exists.".format(self.json_path))
            if not os.path.exists("JSON"):
                os.makedirs("JSON")
            return False

    def json_to_dict(self):
        with open(self.json_path) as json_f:
            data = json.load(json_f)
        return data

    def check_for_new_articles(self):
        self.new_articles = {}
        if self.json_exists():
            json_data = self.json_to_dict()
            for id, data in self.scrapped_articles.items():
                if id not in json_data:
                    self.new_articles[id] = data
            logging.info("Found {} new article(s) on {}.".format(len(self.new_articles),self.name))

    def save_articles_to_json(self):
        with open(self.json_path, 'w') as outputfile:
            json.dump(self.scrapped_articles, outputfile, sort_keys=True, indent=4, separators=(',', ': '))
        logging.debug("File '{}' saved.".format(self.json_path))

    def run(self):
        self.parse_html()
        if not self.parsed_html:
            return None

        try:
            self.scrap_website()
        except AttributeError as e:
            logging.warning("{} cannot be scrapped.".format(self.name))
            raise e

        if self.scrapped_articles:
            self.check_for_new_articles()
            self.save_articles_to_json()

        return self
        