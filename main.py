from Offerter.offerters import Gratka, Gumtree, Otodom, Morizon, Oferty, Nieruchomosci, Olx
from emails import Email

PRICE_TO = 200000

mieszkania = [Otodom("https://www.otodom.pl/sprzedaz/mieszkanie/dabrowa-gornicza/?search%5Bfilter_float_price%3Ato%5D={price_to}&search%5Bdescription%5D=1&search%5Border%5D=created_at_first%3Adesc&search%5Bsubregion_id%5D=302&search%5Bcity_id%5D=134&nrAdsPerPage=72".format(price_to=PRICE_TO)),
            Gratka("https://gratka.pl/nieruchomosci/mieszkania/dabrowa-gornicza/wtorny?cena-calkowita:max={price_to}".format(price_to=PRICE_TO)),
            Morizon("https://www.morizon.pl/mieszkania/najnowsze/dabrowa-gornicza/?ps%5Bprice_to%5D={price_to}".format(price_to=PRICE_TO)),
            Oferty("https://www.oferty.net/mieszkania/szukaj?ps%5Badvanced_search%5D=1&ps%5Btype%5D=1&ps%5Bfavourites%5D=0&ps%5Blocation%5D%5Btype%5D=1&ps%5Blocation%5D%5Btext_queue%5D%5B%5D=D%C4%85browa+G%C3%B3rnicza&ps%5Btransaction%5D=1&ps%5Bprice_to%5D={price_to}&ps%5Bdate_filter%5D=0&ps%5Bsort_order%5D=added_at_desc".format(price_to=PRICE_TO)),
            Gumtree("https://www.gumtree.pl/s-mieszkania-i-domy-sprzedam-i-kupie/dabrowa-gornicza/v1c9073l3200281p1?pr=,{price_to}".format(price_to=PRICE_TO)),
            Nieruchomosci("https://www.nieruchomosci-online.pl/szukaj.html?3,mieszkanie,sprzedaz,,D%C4%85browa%20G%C3%B3rnicza:47529,,,,-{price_to}&o=modDate,desc".format(price_to=PRICE_TO)),
            Olx("https://www.olx.pl/nieruchomosci/mieszkania/sprzedaz/dabrowa-gornicza/?search%5Bfilter_float_price%3Ato%5D={price_to}&search%5Border%5D=created_at%3Adesc&view=list".format(price_to=PRICE_TO))]


def scrap_them(websites, send_to_address):
    errors = []
    body = ""
    for website in websites:
        try:
            website.run()
            if website.new_articles:
                body = body + "{}".format(website)
        except AttributeError as e:
            errors.append("{}\t{}".format(website.name, e))

    email = Email()
    if body:
        email.send(send_to_address, "Nowe oferty!", body)
    if errors:
        email.send('blakes22@gmail.com', "Scrapping errors!", "\n".join(errors))

scrap_them(mieszkania, 'blakes22@gmail.com')

